package dragonstone.demo;

import javax.crypto.SecretKey;

public class EntryPoint {

    // Parameters aesKey and name are free (represented by taking
    // them as inputs)
    public static void main(SecretKey aesKey, String name) throws Exception {
	S3UploadUtils.upload(aesKey, name, getTaintedBytes());
    }

    public static byte[] getTaintedBytes() {
	return new byte[4];
    }
    
}
