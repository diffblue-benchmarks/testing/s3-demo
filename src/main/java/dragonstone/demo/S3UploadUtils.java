package dragonstone.demo;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import java.util.Base64;

public class S3UploadUtils {
    public static void upload(SecretKey aesKey, String name, byte[] bytes) throws Exception {
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, aesKey);
        AmazonS3 s3 = AmazonS3ClientBuilder.defaultClient();

        try {
            String ciphertext = Base64.getEncoder().encodeToString(cipher.doFinal(bytes));
            s3.putObject("example-bucket", name, ciphertext);
        } catch (Exception e) {
            // S3 threw an exception. Try one more time.
            String ciphertext = Base64.getEncoder().encodeToString(bytes);
            s3.putObject("example-bucket", name, ciphertext);
        }
    }
}
